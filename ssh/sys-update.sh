#!/usr/bin/env bash

echo "Pulling git repository"

sudo git -C /opt/the-core-smart-node/ reset --hard

sudo git -C /opt/the-core-smart-node/ pull

sudo find /opt/the-core-smart-node/ssh/ ! -name sys-update.sh -exec chmod u+x {} \;

echo "Update complete!" 
echo "Running main script..."

sudo bash /opt/the-core-smart-node/ssh/main.sh