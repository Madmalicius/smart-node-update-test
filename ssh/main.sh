#!/usr/bin/env bash

echo "Testing main code..."

if [ 2 -eq 2 ]; then
	echo "2 = 2"
fi

echo "Running script..."

while true; do
	if [[ "$(git -C /opt/the-core-smart-node/ rev-parse HEAD)" =~ "$(git -C /opt/the-core-smart-node/ rev-parse origin)" ]]; then
		echo "no new updates"
		sleep 10
	else
		echo "Updating system..."
		sudo bash /opt/the-core-smart-node/ssh/sys-update.sh
	fi
done

echo "OUT OF LOOP"