#!/usr/bin/env bash

echo "Setting timezone..."

sudo ln -sf /usr/share/zoneinfo/CET /etc/localtime

echo "Editing daily command..."

#sudo sed -i -e '$i 0 14 * * * root bash /opt/the-core-smart-node/ssh/update-scheduler.sh' /etc/crontab
(sudo crontab -l ; echo "0 4 * * * git -C /opt/the-core-smart-node/ fetch &") | sudo crontab -

echo "installing git..."

whoami

sudo apt-get update

sudo apt-get -y install git

echo "cloning gitLab repository..."

sudo git clone https://gitlab.com/Madmalicius/smart-node-update-test.git /opt/the-core-smart-node/

echo "opening program..."

sudo chmod u+x /opt/the-core-smart-node/ssh/

sudo sed -i -e '$i \sudo bash /opt/the-core-smart-node/ssh/sys-update.sh\n' /etc/rc.local 

bash /opt/the-core-smart-node/ssh/main.sh