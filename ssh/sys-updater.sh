#!/usr/bin/env bash

echo "fetching git master..."

sudo git -C /opt/the-core-smart-node/ fetch

if [[ ! ("$(git -C /opt/the-core-smart-node/ rev-parse HEAD)" =~ "$(git -C /opt/the-core-smart-node/ rev-parse origin)") ]]; then

    echo "Pulling git repository"

    sudo pkill -9 -f /opt/the-core-smart-node/python/main.py

    sudo git -C /opt/the-core-smart-node/ reset --hard

    sudo git -C /opt/the-core-smart-node/ pull

    sudo find /opt/the-core-smart-node/ssh/ ! -name sys-update.sh -exec chmod u+x {} \;

    echo "Update complete!" 
    echo "Running main script..."

    sudo python /opt/the-core-smart-node/python/main.py
fi