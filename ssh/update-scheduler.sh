#!/usr/bin/env bash

echo "fetching repository..."

sudo git -C /opt/the-core-smart-node/ fetch

echo "relaunching main script"

sudo bash /opt/the-core-smart-node/ssh/main.sh