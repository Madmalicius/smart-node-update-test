#!/usr/bin/env bash

echo "Setting timezone..."

sudo ln -sf /usr/share/zoneinfo/CET /etc/localtime

echo "installing git..."

sudo apt-get update

sudo apt-get -y install python3-gpiozero

sudo apt-get -y install git

echo "cloning gitLab repository..."

sudo git clone https://gitlab.com/Madmalicius/smart-node-update-test.git /opt/the-core-smart-node/

sudo chmod u+x /opt/the-core-smart-node/ssh/

sudo sed -i -e '$i \sudo bash /opt/the-core-smart-node/ssh/sys-updater.sh\n' /etc/rc.local 

echo "Editing daily command..."

(sudo crontab -l ; echo "* * * * * bash /opt/the-core-smart-node/ssh/sys-updater.sh &") | sudo crontab -

echo "opening program..."

sudo python /opt/the-core-smart-node/python/main.py